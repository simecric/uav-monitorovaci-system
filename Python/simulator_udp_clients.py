import logging
import threading
import time
import socket



host = "xxx.xxx.xxx.xxx" #ip adresa serveru
port = 8094 #port

def sim_client(name):#funkce simulující jednoho klienta odesílajícího zprávu v jednom vlákně
    logging.info("Client %s started", name)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    for x in range(250):#tento cyklus udává počet odeslaných zpráv klientem
        string="udp_test,thrd=" + str(name) + " number=" + str(x+int(name))# složení testovací zprávy s názvem pěření "udp_test", číselným identifikátorem vlákna "thrd" a hodnotami "number"
        s.sendto(string.encode('utf-8'),(host, port)) #zakódování zprávy do binární podoby a odeslání paketu na server
        time.sleep(1)#zpráva se odesílá každou sekundu
    s.close()
    logging.info("Client %s: finishing", name)

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S") #nastavení tvaru zprávy vypisování do konzole

    threads = list()
    for index in range(100):# tento for cyklus udává počet simulovaných klientů
        logging.info("Main    : starting thread %d.", index)
        x = threading.Thread(target=sim_client, args=(index,))
        threads.append(x)
        x.start()

    for index, thread in enumerate(threads):
        logging.info("Main    : ending thread %d.", index)
        thread.join()
        logging.info("Main    : client %d finished", index)
