/*
Program byl převzat z ukázky 0.4 Communication -> MultiSerial
*/


void setup() {
  //inicializace obou sériových portů, zde jsem nastavoval postupně různé rychlosti; 9600, 19200, 38400, 57600, 74880, 115200, 230400
  Serial.begin(9600);
  Serial1.begin(9600);
}

void loop() {
  //zde probíhá výpis z jedné sériové linky na druhou a naopak
  if (Serial1.available()) {
    int inByte = Serial1.read();
    Serial.write(inByte);
  }

  if (Serial.available()) {
    int inByte = Serial.read();
    Serial1.write(inByte);
  }
}
