#include <MKRGSM.h>

#include "arduino_secrets.h" // separátní soubor s informacemi pro přihlášení k mobilní síti

const char PINNUMBER[]     = SECRET_PINNUMBER;
const char GPRS_APN[]      = SECRET_GPRS_APN;
const char GPRS_LOGIN[]    = SECRET_GPRS_LOGIN;
const char GPRS_PASSWORD[] = SECRET_GPRS_PASSWORD;

GSMSSLClient client;
GPRS gprs;
GSM gsmAccess(true);// true zajistí výpis informací z modemu na sériovou linku

String hostName = "www.google.com";
int pingResult;

void setup() {
  Serial.begin(9600);
  while (!Serial) { //čekání na připojení USB sériové linky
    ;
  }

  Serial.println("Starting Arduino GPRS ping.");
  bool connected = false;


  while (!connected) {// připojení se k síti operátora za pomoci přihlašovacích údajů
    Serial.println(connected);
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &&
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      connected = true;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }
  Serial.println("Connected");
}

void loop() {
  float avg_time = 0;
  float avg_speed = 0;
  for (int i = 0; i < 1000; i++) {// provedení 1000 měření
    Serial.print("Pinging ");
    Serial.print(hostName);
    Serial.print(": ");


    pingResult = gprs.ping(hostName);

    if (pingResult >= 0) {
      Serial.println(i);
      Serial.print("Doba odezvy = ");
      Serial.print(pingResult);
      Serial.println(" ms");
      avg_time += pingResult;
      float delta_speed = 86.0 / (pingResult / 1000.0);
      Serial.print("Rychlost přenosu = ");
      Serial.println(delta_speed);
      Serial.println(" B/s");
      avg_speed += delta_speed;
    } else {
      Serial.print("Error: ");
      Serial.println(pingResult);
      break;
    }

    delay(500);
  }

  Serial.println("Done");
  Serial.println("Průměrná odezva [s]?");
  Serial.println(avg_time / 1000);
  Serial.println("Průměrná rychlost přenosu [B/s]");
  Serial.println(avg_speed / 1000);


  while (true) {
    delay(10000);
  }
}
