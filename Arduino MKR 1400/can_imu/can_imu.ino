#include <MKRGSM.h>
#include <CAN.h>

#include "arduino_secrets.h" // separátní soubor s informacemi pro přihlášení k mobilní síti

const char PINNUMBER[]     = SECRET_PINNUMBER;
const char GPRS_APN[]      = SECRET_GPRS_APN;
const char GPRS_LOGIN[]    = SECRET_GPRS_LOGIN;
const char GPRS_PASSWORD[] = SECRET_GPRS_PASSWORD;


IPAddress influxdb(xxx, xxx, xxx, xxx); //IP adresa serveru

GSMClient client;
GPRS gprs;
GSM gsmAccess;

GSMUDP Udp;

int data_bytes[10][9];

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(9600);


  if (!CAN.begin(1000E3)) {
    Serial.println("Starting CAN failed!");
    while (1);
  }
  Serial.println("CAN initialized");

  bool connected = false;


  while (!connected) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &&
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      connected = true;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }
  Udp.begin(8094);
  
}

void loop() {
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);

  CAN.beginPacket(0x1);//požadavek dat z IMU jednotky
  CAN.endPacket();

  int i = 0;
  int j = 0;
  while (1) {
    if (CAN.parsePacket()) {//přijmutí a parsování zprávy
      data_bytes[i][0] = CAN.packetId();//uložení ID zprávy
      while (CAN.available()) {
        data_bytes[i][1 + j] = CAN.read();//uložení jednotlivých bytů
        j++;
      }
      j = 0;
      i++;
    }
    if (CAN.packetId() == 1303) {//pokud je přijata zpráva s posledním ID, smyčka je přerušena
      break;
    }
  }

  for (int k = 0; k < 1; k++) {//odeslání přijatých dat
    for (int l = 0; l < 9; l++) {
      String message = "let_senzor,id=" + String(data_bytes[k][0]) + ",byte=" + String(l) + " data=" + String(data_bytes[k][l + 1]);//složení zprávy kde název měření je "let_senzor", následuje "id" a poté o který byte v pořadí CAN zprávy se jedná, value je hodnota byte
      Serial.println(message); 

      byte plain[message.length() + 1];
      message.getBytes(plain, message.length() + 1);

      Udp.beginPacket(influxdb, 8094);
      Udp.write(plain, sizeof(plain) - 1);
      Udp.endPacket();
      Serial.println("Data sent");
    }
    Serial.println("--------------");
  }

  digitalWrite(LED_BUILTIN, LOW);
}
