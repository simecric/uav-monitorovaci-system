#include <MKRGSM.h>
#include <Arduino_MKRENV.h>
#include <Arduino_MKRGPS.h>
#include <SPI.h>
#include <SD.h>
#include <TimeLib.h>

#include "arduino_secrets.h" // separátní soubor s informacemi pro přihlášení k mobilní síti

const char PINNUMBER[]     = SECRET_PINNUMBER;
const char GPRS_APN[]      = SECRET_GPRS_APN;
const char GPRS_LOGIN[]    = SECRET_GPRS_LOGIN;
const char GPRS_PASSWORD[] = SECRET_GPRS_PASSWORD;

const int chipSelect = 4;
unsigned long epoch = 1;
unsigned long old_epoch = 0;
int timedif = 0;

IPAddress influxdb(30, 25, 123, 123); // ip adresa serveru

GSMClient client;
GPRS gprs;
GSM gsmAccess(true);// true zajistí výpis podrobnějších informací z modemu na sériovou linku

GSMUDP Udp;

float pressure = 0;

String temp;
String hum;
String pres;
String illu;
String gps_lat;
String gps_lon;
String gps_speed;
String gps_alt;
String gps_sat;
String atm_press_cal;

String sd_data;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(9600);

  if (!ENV.begin()) {// inicializace ENV shieldu
    Serial.println("Failed to initialize MKR ENV Shield!");
    while (1);
  }


  if (!GPS.begin()) {// inicializace GPS shieldu
    Serial.println("Failed to initialize GPS!");
    while (1);
  }

  if (!SD.begin(chipSelect)) {//inicializace SD karty
    Serial.println("Card failed, or not present");
    while (1);
  }

  Serial.println("Starting Arduino GPRS client.");

  bool connected = false;

  while (!connected) {// připojení se k síti operátora za pomoci přihlašovacích údajů
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &&
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      connected = true;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

  Udp.begin(8094); // nastavení portu na které se budou zasílat UDP pakety



  Serial.println("Started");
}


void loop() {

  Serial.println("Starting measuremnt");

  //následující sekce kontroluje a postupně tvoří soubory
  String filename = "log";
  int lognum = 0;
  filename += String(lognum);
  while (SD.exists((filename + ".txt"))) {
    filename.remove(filename.length() - String(lognum).length(), String(lognum).length());
    filename += String(++lognum);
    Serial.println(filename);
  }
  filename += ".txt";
  Serial.println("Name found: " + filename);
  File dataFile = SD.open(filename, FILE_WRITE);

  int i = 0;

  if (dataFile) {
    Serial.println("File created");

    while (true) {
      if (GPS.available()) {

        epoch = GPS.getTime();
        timedif = epoch - old_epoch;


        if ((timedif > 1) && (i >= 1)) {
          for (int j = 1; j <= timedif; j++) {//doplnění nul pokud se ztratí GPS signál. Doplní se tolik řádků nul, kolik sekund signálu vypadlo
            dataFile.println("0");
          }
          Serial.println("Skipped: " + String(timedif - 1));
        }
        else {

          digitalWrite(LED_BUILTIN, HIGH);   // Pro indikaci čtení dat a přenosu se rozsvítí indikační led dioda na desce

          temp = String(ENV.readTemperature());
          hum = String(ENV.readHumidity());
          pressure = ENV.readPressure();

          pres = String(pressure);

          illu = String(ENV.readIlluminance());


          gps_lat = String(GPS.latitude());
          gps_lon = String(GPS.longitude());
          gps_speed = String(GPS.speed());
          gps_alt = String(GPS.altitude());
          gps_sat = String(GPS.satellites());
          atm_press_cal = String(-2307.69 * (pow(pressure / 1013.25, 0.1902) - 1));



          sendData("env_data temperature=" + temp);
          sendData("env_data humidity=" + hum);
          sendData("env_data pressure=" + pres);
          sendData("env_data illuminance=" + illu);

          sendData("GPS_data lat=" + gps_lat);
          sendData("GPS_data lon=" + gps_lon);
          sendData("GPS_data ground_speed=" + gps_speed );
          sendData("GPS_data altitude=" + gps_alt);
          sendData("GPS_data sat_num=" + gps_sat);

          sendData("env_data atm_press=" + atm_press_cal);


          sd_data = temp + "," + hum + "," + pres + "," + illu + "," + gps_lat + "," + gps_lon + "," + gps_speed + "," + gps_alt + "," + gps_sat + "," + atm_press_cal;

          dataFile.println(sd_data);


          Serial.println("Data sent");
          Serial.println("-----");
          digitalWrite(LED_BUILTIN, LOW);

          i++;
        }
        if (i % 10 == 0) {//každých 10 záznamů se data zapíší na sd kartu
          dataFile.flush();
          Serial.println("Flushed");
        }
        old_epoch = epoch;

      }
    }
    dataFile.close();
  }

  else {
    Serial.println("Error opening " + filename); //pokud dojde k chybě tvorby souboru, vypíše se error
  }
}

void sendData(String message) {//  funkce pro odesílání UDP zprávy
  //Serial.println(message);
  byte plain[message.length() + 1];
  message.getBytes(plain, message.length() + 1);
  Udp.beginPacket(influxdb, 8094);
  Udp.write(plain, sizeof(plain) - 1);
  Udp.endPacket();
}
